const express = require('express')
const route = express.Router()
const controller = require('../controller/controller')
const axios = require('axios')
const { response } = require('express')

// home 
route.get('/dashboard', (req,res) => {
    axios.get('http://localhost:7000/api-user')
    .then(response => {
        res.render('index', {userdata: response.data})
    })
    .catch(err => {
        res.send(err)
    })
})
// login
route.get('/', (req,res) => {
    axios.get('http://localhost:7000/api-user')
    .then(response => {
        res.render("login")
    })
    .catch(err => {
        res.send(err)
    })
})
// tambah pengguna
route.get('/tambah-pengguna', (req,res) => {
    res.render('tambah_pengguna')
})
// update
route.get('/update-user', (req,res) => {
    axios.get('http://localhost:7000/api-user', {params: {id: req.query.id} })
    .then((response) =>{
        res.render("update_pengguna", {userdata: response.data})
    })
    .catch(err => {
        res.send(err)
    })
})
// profile
route.get('/detail-profile', (req,res) => {
    axios.get('http://localhost:7000/api-profile', {params: {username: req.query.username} })
    .then((response) =>{
        res.render("detail_user", {userdata: response.data})
    })
    .catch(err => {
        res.send(err)
    })
})

// API user
route.post('/api-user', controller.regis)
route.post('/login', controller.login)
route.get('/api-user',controller.findUser)
route.put('/api-user/:id', controller.updateUser)
route.get('/api-profile',controller.findProfile)
route.put('/api-profile/:id', controller.detailProfile)
route.delete('/api-user/:id', controller.deleteUser)
// game score
route.post('/api/game-score',controller.gameScore)
route.get('/api/game-score',controller.gameRead)

module.exports = route