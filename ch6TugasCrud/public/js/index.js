//login
$("#login").submit((e)=>{
    alert("success to login ...")
})

// insert
$("#tambah_pengguna").submit( (event) => {
    alert("data berhasil ditambahkan...")
})

//update
$("#update_pengguna").submit( (event) => {
    event.preventDefault()

    let unindexed_array = $("#update_pengguna").serializeArray()
    let data = {}

    $.map(unindexed_array, (n, i) => {
        data[n['name']] = n['value']
    })

    let request = {
        "url": `/api-user/${data.id}`,
        "method": "PUT",
        "data": data
    }

    $.ajax(request).done( (response) => {
        alert("data berhasil diupdate...")
        window.location.href = "/dashboard"
    })
})
// profile
$("#detail_profile").submit( (event) => {
    event.preventDefault()

    let unindexed_array = $("#detail_profile").serializeArray()
    let data = {}
    
    $.map(unindexed_array, (n, i) => {
        data[n['name']] = n['value']
    })

    let request = {
        "url": `/api-profile/${data.id}`,
        "method": "PUT",
        "data": data
    }

    $.ajax(request).done( (response) => {
        alert("data berhasil diupdate...")
        window.location.href = "/dashboard"
    })
})
//delete
if (window.location.pathname == "/dashboard") {
    $(".btn.border-shadow.delete").on('click', function(e) {
        console.log($(this).attr('dataId'))

        let request = {
            "url": `/api-user/${$(this).attr('dataId')}`,
            "method": "DELETE"
        }

        if (confirm("apakah anda yakin ?")) {
            $.ajax(request).done( (response) => {
                alert("data berhasil dihapus")
                location.reload()
            })
        }
    })
}
