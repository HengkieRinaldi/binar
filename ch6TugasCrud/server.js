const express = require('express');
const app = express()
const dotenv = require('dotenv');
const morgan = require('morgan');
const path = require('path')
const bodyparser = require('body-parser')
const koneksiDB = require('./model/connection')

//middleware
dotenv.config( { path : './config/config.env'})
const PORT = process.env.PORT

app.use(morgan('dev'))

// koneksi
koneksiDB()
app.use(bodyparser.urlencoded({extended : true}))

app.set("view engine","ejs")
app.use('/css',express.static(path.resolve(__dirname, "public/css")))
app.use('/js',express.static(path.resolve(__dirname, "public/js")))
app.use('/', require('./routes/router'))
app.listen(PORT, ()=> {console.log(`server running on http://localhost:${PORT}`)})