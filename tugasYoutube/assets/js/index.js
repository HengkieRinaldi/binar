// tambah
$("#add_user").submit(function (event) {
    alert("Data Inserted Successfully!");
})
// update
$("#update_user").submit(function (event) {
    event.preventDefault();
    alert("test")
    let unindexed_array = $("#update_user").serializeArray();
    let data = {}

    $.map(unindexed_array, function (n, i) {
        data[n['name']] = n['value']
    })
    console.log(data)

    // let request = {
    //     "url": `http://localhost:7000/api/users/${data.id}`,
    //     "method": "PUT",
    //     "data": data
    // } 

    // $.ajax(request).done(function (response) {
    //     alert("Data telah diupdate!")
    //     console.log(data)
    // })
})
// delete
if (window.location.pathname == "/") {
    $ondelete = $(".table tbody td a.delete");
    $ondelete.click(function () {
        let id = $(this).attr("data-id")

        let request = {
            "url": `http://localhost:7000/api/users/${id}`,
            "method": "DELETE"
        }

        if (confirm("apakah anda yakin ?")) {
            $.ajax(request).done(function (response) {
                alert("Data telah dihapus!");
                location.reload();
            })
        }

    })
}