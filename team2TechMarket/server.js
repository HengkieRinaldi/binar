const express = require('express')
const app = express()
const dotenv = require('dotenv')
const morgan =  require('morgan')
const path = require('path')
const { urlencoded } = require('express')
const connectionDB = require('./config/connection')

//middleware
dotenv.config({path : './config/config.env'})
const PORT = process.env.PORT
app.use(morgan('dev'))
app.use(urlencoded({extended :true}))
// connect db
connectionDB()
app.use('/',require('./route/router'))
app.listen(PORT,() => {console.log(`server running on http://localhost:${PORT}`)})