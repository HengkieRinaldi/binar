const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    title: { type : String, required: true},
    picture: { type : Buffer},
    author: { type : String, required: true},
    description: { type : String, required: true},
    timeStamp: { type : Date},
})

const news = mongoose.model('news', schema)
module.exports = news