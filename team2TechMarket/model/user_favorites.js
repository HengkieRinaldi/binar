const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    user_id: { type : String, required: true},
    product_id: { type : String, required: true},
    data_date: { type : Date, required: true},
})

const user_favorites = mongoose.model('user_favorites', schema)
module.exports = user_favorites