const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    first_name: { type : String, required: true},
    last_name: { type : String, required: true},
    username: { type : String, required: true},
    email: { type : String, required: true},
    password: { type : String, required: true},
    phone: { type : Number, required: true},
    gender: { type : String, required: true},
    birth: { type : Date, required: true},
    address: { type : String, required: true},
    type_user: { type : String, required: true},    
})

const user = mongoose.model('user', schema)
module.exports = user