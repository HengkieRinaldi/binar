const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    product_id: { type : String, required: true},
    user_id: { type : String, required: true},
    username: { type : String, required: true},
    rating: { type : Number, required: true},
    comment: { type : String, required: true},
    timeStamp: { type : Date},
})

const review = mongoose.model('review', schema)
module.exports = review