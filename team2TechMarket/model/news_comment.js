const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    news_id: { type : String, required: true},
    user_id: { type : String, required: true},
    username: { type : String, required: true},
    comment: { type : String, required: true},
    timeStamp: { type : Date},
})

const news = mongoose.model('news', schema)
module.exports = news