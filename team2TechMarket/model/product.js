const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    title: { type : String, required: true},
    picture: { type : Buffer},
    price: { type : Number, required: true},
    stock: { type : Number, required: true},
    description: { type : String, required: true},
    category: { type : String},
    rating_avg: { type : Number},
    timeStamp: { type : Date},
})

const product = mongoose.model('product', schema)
module.exports = product