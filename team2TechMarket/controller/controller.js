const Cryptr = require('cryptr')
const secretkey = 'secretkey'
const cryptrConverter = new Cryptr(secretkey)
const jwt = require('jsonwebtoken')
const user = require('../model/user')
// const profile = require('../model/profile')

// register
exports.register = async (req, res) => {
    let { first_name, last_name, username, email, password, phone, gender, birth, address } = req.body
    if( !first_name || !last_name || !username || !email || !password || !phone || !gender || !birth || !address ) {
        res.status(400).send({
            message: "wrong datas",
            statusCode: 400
        })
    } else {
        try {
            // check data from database
            let userCheck = await user.findOne({ username:username, email:email })
            if (userCheck) {
                res.status(400).send({
                    message: "Username or Email has Registered.",
                    statusCode: 400
                })
            } else {
                // encrypt password
                let newPassword = cryptrConverter.encrypt(password)
                // create new user
                let createUser = await user.create({
                    first_name: first_name, last_name: last_name, username: username, email: email,
                    password: newPassword, phone: phone, gender: gender, birth: birth, address: address
                })
                console.log(createUser)
                // // create profile
                // let createProfile = await profile.create({
                //     user_id: createUser._id,
                //     username: createUser.username,
                //     firstName: '',
                //     lastName: '',
                //     fullName: '',
                //     umur: 0,
                //     tglLahir: '',
                //     gender: '',
                //     address: ''
                // })
                // console.log(createProfile)
            }
            // send info
            res.status(200).send({
                message: "success to create data..",
                statusCode:200
            })
        } catch (err) {
            console.log(err)
        }
    }
}
// get user
exports.findUser = async (req, res) => {
    if(req.query.id) {
        const id = req.query.id
        let findId = await user.findById(id)
        try {
            if(!findId) {
                res.status(400).send({ message: "data not found"})
            } else {
                res.send(findId)
            }
        } catch (err) {
            console.log(err)
        }
    } else {
        try {
            let getUser = await user.find()
            res.send(getUser)
        } catch (err) {
            res.status(500).send({message: err.message})
        }
    }
}