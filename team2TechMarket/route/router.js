const express = require('express')
const route = express.Router()
const controller = require('../controller/controller')
const axios = require('axios')
const { response } = require('express')


// API
route.get('/api/user', controller.findUser)
route.post('/api/user', controller.register)

module.exports = route