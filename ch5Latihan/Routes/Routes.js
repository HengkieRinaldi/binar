const express = require('express')
const app = express()
const Routes = express.Router()
var user = { username: "hengki", password:1234}

var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
    function (username, password, done) {
        User.findOne({ username: username }, function (err, user) {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, { message: 'Incorrect username.' });
            }
            if (!user.validPassword(password)) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            return done(null, user);
        });
    }
));

// data
let dataSiswa = [
    {
        no: 1,
        nama: 'hengki',
        noInduk: '101122',
        jurusan: 'matematika',
        alamat: 'bandung'
    }
]
let data2 = [
    {
        id: 1,
        nama: 'hengki',
        pekerjaan: 'pelajar',
        status: 'belum menikah'
    }
]

// fungsi
Routes.get('/', function (req, res) {
    res.render('index')
})

Routes.get('/login', (req,res) => {
    res.render('login')
})

Routes.post('/login',
passport.authenticate('local', { successRedirect: '/',
failureRedirect: '/login',
failureFlash: true })
);

// Routes.get('/siswa', function (req, res) {
//     let dataSiswa = []
//     for (i= 0; i < 10; i++)
//     dataSiswa[i] = [
//         {
//             no: i,
//             nama: 'hengki'+i,
//             noInduk: '101122' +i,
//             jurusan: 'matematika' +i,
//             alamat: 'bandung' +i
//         }
//     ]
//     res.send(dataSiswa)
// })

// Routes.post('/pekerjaan', (req, res) => {
//     let newJob = {
//         id: data2.length + 1,
//         nama: req.nama,
//         pekerjaan: req.pekerjaan,
//         status: req.status,
//     };

//     data2.push(newJob);
//     res.json(newJob);
//     console.log(newJob)
// });
module.exports = Routes