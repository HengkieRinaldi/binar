const mongoose = require('mongoose')

let schema = new mongoose.Schema({
    username : { type : String, unique: true, required: true},
    password : { type : String, required: true},
    email : { type : String, required: true},
    namaLengkap : { type : String, required: true},
    umur : { type : String, required: true},
    jenisKelamin : { type : String, required: true},
    alamat : { type : String, required: true},
})

const Pengguna = mongoose.model('pengguna', schema)

module.exports = Pengguna