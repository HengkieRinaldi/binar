const express = require('express')
const route = express.Router()
const controller = require('../controller/controller')
const axios = require('axios')
const { response } = require('express')
const connectEnsureLogin = require('connect-ensure-login')

// home 
route.get('/', (req,res) => {
    axios.get('http://localhost:7000/api-user')
    .then(response => {
        res.render("index", {userdata: response.data})
    })
    .catch(err => {
        res.send(err)
    })
})
// login
route.get('/login', (req,res) => {
    axios.get('http://localhost:7000/api-user')
    res.render("login")
})
// game
route.get('/game', (req,res) => {
    res.render("game")
})

//dashboard
route.get('/dashboard', (req,res) => {
    axios.get('http://localhost:7000/api-user')
    .then(response => {
        res.render('dashboard', {userdata: response.data})
    })
    .catch(err => {
        res.send(err)
    })
})

// tambah pengguna
route.get('/tambah-pengguna', (req,res) => {
    res.render('tambah_pengguna')
})

// update
route.get('/update-user', (req,res) => {
    axios.get('http://localhost:7000/api-user', {params: {id: req.query.id} })
    .then((datanya) => {
        res.render("update_pengguna", {pemilik: datanya.data})
    })
    .catch(err => {
        res.send(err)
    })
})

route.get('/logout', function(req, res) {
    req.logout()
    res.redirect('/login')
  })

// user
route.post('/api-user', controller.regis)
route.post('/login',controller.login)
route.get('/api-user',controller.findUser)
route.put('/api-user/:id', controller.updateUser)
route.delete('/api-user/:id', controller.deleteUser)
// game score
route.post('/api/game-score',controller.gameScore)
route.get('/api/game-score',controller.gameRead)
route.post("/api-test",controller.updateUser)

module.exports = route